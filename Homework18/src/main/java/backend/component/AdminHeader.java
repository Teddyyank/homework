package backend.component;

import backend.core.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utils.Browser;

public class AdminHeader extends Base {

    private static final By USER_FULLNAME_IN_HEADER = By.cssSelector("ul.navbar-nav li.dropdown > a");

    /**
     * Asserts if the fullname in the header next to the logout button is as we expect
     *
     * @param expectedFullName the full name we expect to see next to the logout button
     * @param messageOnFailure the message that will appear in your reports in case of test failure
     */
    public static void verifyUserFullName(String expectedFullName, String messageOnFailure) {
        String actualFullName = getText(USER_FULLNAME_IN_HEADER);
        Assert.assertEquals(actualFullName, expectedFullName, messageOnFailure);
    }
}
