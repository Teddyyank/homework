package backend.pages;

import backend.core.Base;
import org.openqa.selenium.By;
import org.testng.Assert;
import utils.Browser;


public class AdminLogin extends Base {

    private static final By USERNAME_FIELD = By.id("input-username");
    private static final By PASSWORD_FIELD = By.id("input-password");
    private static final By LOGIN_BUTTON = By.cssSelector(".btn-primary");
    private static final By LOGIN_FORM_VALIDATION_MESSAGE = By.cssSelector(".alert-danger");

    /**
     * Open the admin login area of shop.pragmatic.bg
     */
    public static void open() {
        Browser.driver.get("http://shop.pragmatic.bg/admin");
    }

    /**
     * Logs into the the admin area of shop.pragmatic.bg
     *
     * @param username the username you would like to login with
     * @param password the password you would like to login with
     */
    public static void login(String username, String password) {
        type(USERNAME_FIELD, username);
        type(PASSWORD_FIELD, password);
        click(LOGIN_BUTTON);
    }

    /**
     * Asserts if the expected login form validation message is as shown in the browser when
     * trying to login with wrong credentials
     *
     * @param expectedLoginFormValidationMessage the validation message you expect to see when u login with wrong credentials
     * @param messageOnFailure the message that will appear in your test reports in case of test failure
     */
    public static void verifyLoginFormValidationMessage(String expectedLoginFormValidationMessage, String messageOnFailure) {
        String actualLoginFormValidationMessage = getText(LOGIN_FORM_VALIDATION_MESSAGE);
        Assert.assertTrue(actualLoginFormValidationMessage.contains(expectedLoginFormValidationMessage), messageOnFailure);
    }
}
