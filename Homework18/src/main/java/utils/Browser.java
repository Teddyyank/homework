package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Browser {

    public static WebDriver driver;

    /**
     * Opens Chrome Browser with default configurations, maximized and has
     * implicit wait of 10 seconds
     */
    public static void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "/home/teddy/tools/chromedriver/chromedriver-87");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    /**
     * Quits the process of the browser
     */
    public static void quit() {
        driver.quit();
    }
}
