package frontend.pages;

import frontend.core.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Browser;

public class StoreLogin extends Base {

    private static final By USERNAME_FIELD = By.id("input-email");
    private static final By PASSWORD_FIELD = By.id("input-password");
    private static final By LOGIN_BUTTON = By.cssSelector("input.btn-primary");
    private static final By LOGIN_FORM_VALIDATION_MESSAGE = By.cssSelector(".alert-danger");
    public static final By ADDRESS_INFO = By.xpath("//*[@id='content']/div[1]/table/tbody/tr/td[1]");
    //public static final By ADDRESS_INFO = By.xpath("//*[@id='content']/div[1]/table/tbody/tr/td[1]/text()[1]");
    public static final By ADDRESS_PAGE = By.xpath("//*[@id='column-right']/div/a[4]");


    /**
     * Open the admin login area of shop.pragmatic.bg
     */
    public static void open() {
        Browser.driver.get("http://shop.pragmatic.bg/index.php?route=account/login");
    }

    public static void addressBookPage() {

        click(ADDRESS_PAGE);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Logs into the the admin area of shop.pragmatic.bg
     *
     * @param username the username you would like to login with
     * @param password the password you would like to login with
     */
    public static void login(String username, String password) {
        type(USERNAME_FIELD, username);
        type(PASSWORD_FIELD, password);
        click(LOGIN_BUTTON);

    }



    /**
     * Asserts if the expected login form validation message is as shown in the browser when
     * trying to login with wrong credentials
     *
     * @param expectedLoginFormValidationMessage the validation message you expect to see when u login with wrong credentials
     * @param messageOnFailure                   the message that will appear in your test reports in case of test failure
     */
    public static void verifyLoginFormValidationMessage(String expectedLoginFormValidationMessage, String messageOnFailure) {
        String actualLoginFormValidationMessage = getText(LOGIN_FORM_VALIDATION_MESSAGE);
        Assert.assertTrue(actualLoginFormValidationMessage.contains(expectedLoginFormValidationMessage), messageOnFailure);

    }

    public static void verifyAddress(String expectedAddress) {
        //String actualAddress = getText(ADDRESS_INFO);
        String actualAddress = getText(ADDRESS_INFO);
        //Assert.assertEquals(actualAddress, expectedAddress);
        Assert.assertTrue(actualAddress.contains(expectedAddress));
    }

}
