package backend.negative;

import backend.pages.AdminLogin;
import core.BaseTest;
import org.testng.annotations.Test;

public class AdminLoginTests extends BaseTest {

    @Test
    public void unsuccessfulLogin() {
        AdminLogin.open();
        AdminLogin.login("dasfasfasdf", "dasfasdfasdf");
        AdminLogin.verifyLoginFormValidationMessage("No match for Username and/or Password.",
                "there was no validation upon a try to login with wrong credentials");
    }


}
