package backend.positive;

import backend.component.AdminHeader;
import backend.pages.AdminLogin;
import core.BaseTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.Browser;


public class AdminLoginTests extends BaseTest {



    @Test
    public void successfulLogin() {
        AdminLogin.open();
        AdminLogin.login("admin", "parola123!");
        AdminHeader.verifyUserFullName("Milen Strahinski", "it did not login successfully for some reason");
    }



}
