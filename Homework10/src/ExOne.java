import java.util.*;

public class ExOne {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            System.out.printf("Enter new element #%d: ", i);
            numbers.add(sc.nextInt());
            Collections.sort(numbers);
        }

        //check if divisible by 3
        for (Integer smallest : numbers) {
            if (smallest % 3 == 0) {
                System.out.println("Smallest number divisible by 3 is " + smallest);
                break;
            } else {
                System.out.println("No number divisible by 3 present");
                break;
            }
        }


    }

}
