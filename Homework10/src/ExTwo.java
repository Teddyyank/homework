import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ExTwo {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();

        System.out.println("Enter start number: ");
        Integer firstSecond = sc.nextInt();
        numbers.add(firstSecond);
        numbers.add(firstSecond);

        for (int i = 0; i < 8; i++) {
            numbers.add(numbers.get(i) + numbers.get(i + 1));
        }
        System.out.println(numbers.toString());
    }
}
